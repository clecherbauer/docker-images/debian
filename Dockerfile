FROM debian:stretch-slim

COPY .build /.build
RUN /.build/build.sh

EXPOSE 22

CMD ["/sbin/my_init"]
