#!/bin/bash

set -e

export LC_ALL=C
export DEBIAN_FRONTEND=noninteractive
aptInstall='apt-get install -y --no-install-recommends'

## Fix some issues with APT packages.
## See https://github.com/dotcloud/docker/issues/1024
dpkg-divert --local --rename --add /sbin/initctl
ln -sf /bin/true /sbin/initctl

## Replace the 'ischroot' tool to make it always return true.
## Prevent initscripts updates from breaking /dev/shm.
## https://journal.paul.querna.org/articles/2013/10/15/docker-ubuntu-on-rackspace/
## https://bugs.launchpad.net/launchpad/+bug/974584
dpkg-divert --local --rename --add /usr/bin/ischroot
ln -sf /bin/true /usr/bin/ischroot

#save space by removing doc and use only en locales
echo "path-exclude /usr/share/doc/*\n \
path-include /usr/share/doc/*/copyright\n \
path-exclude /usr/share/man/*\n \
path-exclude /usr/share/groff/*\n \
path-exclude /usr/share/info/*\n \
path-exclude /usr/share/lintian/*\n \
path-exclude /usr/share/linda/*" > /etc/dpkg/dpkg.cfg.d/01_nodoc

echo "path-exclude /usr/share/locale/*\n \
path-include /usr/share/locale/en*\n" > /etc/dpkg/dpkg.cfg.d/02_only_en_locale

apt-get update

##set Timezone
ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

## Install HTTPS support for APT.
$aptInstall apt-transport-https ca-certificates

## Install add-apt-repository
$aptInstall software-properties-common

## Install Service dependencies
$aptInstall runit cron syslog-ng-core logrotate

## Install Service dependencies
$aptInstall libsasl2-2 libsasl2-modules

## Install System defaults
$aptInstall curl \
            fontconfig \
            ghostscript \
            git \
            libxext-dev \
            libxrender-dev \
            libxtst-dev \
            libfontconfig1 \
            libfreetype6 \
            libpng16-16 \
            mysql-client \
            nano \
            openssh-client \
            sed \
            wget \
            gnupg \
            systemd

apt --fix-broken install -y

## Install init process.
cp /.build/bin/my_init /sbin/
mkdir -p /etc/my_init.d
mkdir -p /etc/post.services.init.d
mkdir -p /etc/container_environment
touch /etc/container_environment.sh
touch /etc/container_environment.json
chmod 700 /etc/container_environment

groupadd -g 8377 docker_env
chown :docker_env /etc/container_environment.sh /etc/container_environment.json
chmod 640 /etc/container_environment.sh /etc/container_environment.json
ln -s /etc/container_environment.sh /etc/profile.d/

## Install basic system services
/.build/services/syslog-ng/syslog-ng.sh
/.build/services/cron/cron.sh
/.build/services/firstboot/firstboot.sh

# fix gpg issue - see: https://dev.gnupg.org/T3412
# signing failed: Inappropriate ioctl for device
cp -f /.build/bashrc /root/.bashrc

#cleanup
apt autoremove -y
apt-get clean
rm -Rf /var/lib/apt/lists/*
rm -Rf /tmp/*

find /usr/share/locale -mindepth 1 -maxdepth 1 ! -name 'en' |xargs rm -rf # throws rm error
find /usr/share/doc -depth -type f ! -name copyright|xargs rm -f || true # throws rm error
find /usr/share/doc -empty|xargs rmdir || true
rm -Rf /usr/share/man /usr/share/groff /usr/share/info /usr/share/lintian /usr/share/linda /var/cache/man

rm -Rf /.build